def calculator(a, b, operator):
    # ==============
    if "+" in operator:
        return a+b
    if "-" in operator:
        return a-b
    if "*" in operator:
        return a*b
    if "/" in operator:
        f=a//b
        g=a%b
        if g == 0:
            return f
        if g>0:
            return "%s remainder %s" % (f,g)


    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
